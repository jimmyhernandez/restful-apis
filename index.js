import express from 'express';
import mongoose from 'mongoose';
import bodyparser from 'body-parser';

import routes from './src/routes/crmRoutes';

const app = express();
const PORT = 3025;

//mongoose
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/CRMdb');

app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());

routes(app);

app.use(express.static('public'));

app.get('/', (req,res) => {
  res.send(`Node and express server is running on port ${PORT}`);
});

app.listen(PORT,()=>{
  console.log(`Your server is running on port ${PORT}`);
});